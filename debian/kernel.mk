.PHONY: all clean

include settings.mk

all: build-kernel build-fpga-tools

clean:
	-rm linux-xlnx.tar.lz4
	-rm -rf linux-xlnx

build-kernel:
	git clone --depth=1 -b xilinx-v2018.2 https://github.com/Xilinx/linux-xlnx/
	cp .config linux-xlnx/
	cd linux-xlnx/ && make deb-pkg -j8 ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) LOCALVERSION=$(LOCALVERSION) 2>&1 | tee ../$@.log
	touch build-kernel

build-fpga-tools: build-dtbocfg build-udmabuf

build-dtbocfg: build-kernel
	git clone --recursive --depth=1 -b v0.0.5 git://github.com/ikwzm/dtbocfg-ctrl-dpkg
	cd dtbocfg-ctrl-dpkg && make -f debian/rules binary ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) KERNEL_SRC_DIR=`pwd`/../linux-xlnx 2>&1 | tee ../$@.log
	touch build-dtbocfg

build-udmabuf: build-kernel
	git clone --recursive --depth=1 -b v1.3.2 git://github.com/ikwzm/udmabuf-kmod-dpkg
	cd udmabuf-kmod-dpkg && make -f debian/rules binary kernel_release=$(KERNELRELEASE) arch=$(ARCH) deb_arch=$(DEB_ARCH) kernel_src_dir=`pwd`/../linux-xlnx 2>&1 | tee ../$@.log
	touch build-udmabuf
